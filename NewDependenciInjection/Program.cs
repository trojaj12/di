﻿// See https://aka.ms/new-console-template for more information

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NewDependenciInjection;
using NewDependenciInjection.Services;


var hostBuilder = Host.CreateDefaultBuilder(args);
hostBuilder.ConfigureServices((context, collection) =>
{
    collection.AddSingleton<Logger>();
    collection.AddSingleton<Application>();
    collection.AddScoped<Bonzak>(
        // provider => ActivatorUtilities.CreateInstance<Bonzak>(
        //     provider,
        //     provider.GetService<Application>().Prijmeni,
        //     provider.GetService<Application>().Jmeno
            // )
        );
});

var host = hostBuilder.Build();
var app = host.Services.GetService<Application>();

app.run();

public static class Foo
{
    public static Bonzak CreateScopedBoznakOld(this IServiceProvider provider, string jmeno)
    {
        var bonzak = provider.GetService<Bonzak>();
        bonzak.Jmeno = jmeno;
        return bonzak;
    }
    public static Bonzak CreateScopedBoznak(this IServiceProvider provider, string jmeno)
    {
        var bonzak = provider.GetService<Bonzak>();
        bonzak.Jmeno = jmeno;
        return bonzak;
    }
}