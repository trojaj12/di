using System.ComponentModel.Design;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NewDependenciInjection.Services;

namespace NewDependenciInjection;

public class Application
{
    public string Prijmeni = "Trojak";
    public string Jmeno = "Jan";
    public Services.Logger Logger { get; set; }
    public IServiceProvider ServiceProvider { get; set; }

    public Application(Logger logger, IServiceProvider serviceProvider)
    {
        Logger = logger;
        ServiceProvider = serviceProvider;
    }

    public void run()
    {
        Logger.Log("Starting application");
        
        using (var scope = ServiceProvider.CreateScope())
        {
            var boznak = scope.ServiceProvider.CreateScopedBoznak("Jan");
            boznak.Bonzuj();
            boznak.Bonzuj();
            boznak.Bonzuj();
            Logger.Log("Ending application");
        }

        // Jmeno = "Pepa";
        using (var scope = ServiceProvider.CreateScope())
        {
            var boznak = scope.ServiceProvider.CreateScopedBoznak("Pepa");
            boznak.Bonzuj();
            boznak.Bonzuj();
            boznak.Bonzuj();
            Logger.Log("Ending application");
        }
        
    }
}