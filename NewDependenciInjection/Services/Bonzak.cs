namespace NewDependenciInjection.Services;

public class Bonzak : IDisposable
{
    public Bonzak(Logger logger)
    {
        Logger = logger;
        Console.WriteLine($"Se jmenem {Jmeno}");
    }

    public void Bonzuj()
    {
        Logger.Log($"{Jmeno} se posral {Count++}krat.");
    }

    public Logger Logger { get; set; }
    public string Jmeno { get; set; } = null!;
    public int Count { get; set; } = 0;


    public void Dispose()
    {
        Logger.Log("DISPOSE");
    }
}